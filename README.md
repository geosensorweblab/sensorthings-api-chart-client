# SensorThings API Chart Client Demo

# Step 1: Include dependencies
Include these libraries as follows:
```javascript
	<script src="js/jquery-1.11.3.min.js"></script>
	<script src="js/angular.min.js"></script>
	<script src="js/Chart.Core.min.js"></script>
	<script src="js/Chart.Scatter.min.js"></script>
	<script src="js/NChart.js"></script>
```

# Step 2: Add chart section
```html
<div class="col-lg-6 col-sm-12" id="line-chart" ng-controller="ChartCtrl">
		<div class="panel panel-default">
			<div class="panel-heading">
				{{ chartTitle }}
			</div>

			<div class="panel-body">
				<canvas id="header-canvas" width="640" height="480"></canvas>
				<div id="legend"></div>
			</div>
  </div>
</div>
```

# Step 3: Request Data and Parse data
```javascript
Url = "http://162.244.228.33:8080/OGCSensorThings/v1.0/Datastreams(71596)/Observations?$select=result,phenomenonTime,id";
$http({
		method: 'GET',
		url: Url
	}).success(function(data, status) {
          nextLink.length = 0;
          var observationId;
          var result;
          var phenomenonTime;
          
          nextLink.push(data.nextLink != undefined ? data.nextLink : null);
                                  
                  
          var dataForChart = [];
          for (var i = 0; i < data.value.length; i++) {
                  observationId = data.value[i].id;
                  result = data.value[i].result;
                  phenomenonTime = data.value[i].phenomenonTime;
  
                  dataForChart.push({
                          x: Date.parse(phenomenonTime),
                          y: result
                  });
          }
  }
```
# Step 4: Populate required variables in controller
In your controller, set the DataSource and draw options as you would for a chart
```javascript
var myChart = new Chart(ctx).Scatter([{ label: "Humidity [percent]", 
			data:dataForChart}],{
			showScale: true,
			scaleShowLabels: true,
			scaleShowHorizontalLines: true,
			scaleShowVerticalLines: false,
			scaleLineWidth: 1,
			scaleLineColor: "red",
			scaleGridLineColor: "#999",
			scaleDateFormat: "mm/yyyy",
			scaleTimeFormat: "HH:MM",
			scaleDateTimeFormat: "mmm d, yyyy, hh:MM",
			scaleGridLineWidth: 1,
			useUtc: true,
			scaleType: 'date',
			pointDot: false,
			
			scaleOverride: true,
			scaleSteps: 15,
			scaleStepWidth: 0.5,
			scaleStartValue: 29,
			tooltipTemplate: "<%if (datasetLabel){%><%=argLabel%> <%=datasetLabel%>:<%}%><%=valueLabel%>"
			});
```
# Finally
And your chart should display when you load the page.