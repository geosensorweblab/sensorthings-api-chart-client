

var nextLink = [];
var NChart = angular.module('NChart',[]),
	id = '71596',
	Url = "http://162.244.228.33:8080/OGCSensorThings/v1.0/Datastreams("
	+ id + ")/Observations?$select=result,phenomenonTime,id";
NChart.controller('ChartCtrl', function($scope,$http) {
	$scope.chartTitle = 'Data Stream ID: ' + id;
		$http({
		method: 'GET',
		url: Url
	}).success(function(data, status) {
                nextLink.length = 0;
                var observationId;
                var result;
                var phenomenonTime;
                
                nextLink.push(data.nextLink != undefined ? data.nextLink : null);    
                var dataForChart = [];
                for (var i = 0; i < data.value.length; i++) {
                        observationId = data.value[i].id;
                        result = data.value[i].result;
                        phenomenonTime = data.value[i].phenomenonTime;
        
                        dataForChart.push({
                                x: Date.parse(phenomenonTime),
                                y: result
                        });
                }
				
                console.log(dataForChart);
                var ctx = document.getElementById("header-canvas").getContext("2d");
				
		var myChart = new Chart(ctx).Scatter([{ label: "Humidity [percent]", 
			data:dataForChart}],{
			showScale: true,
			scaleShowLabels: true,
			scaleShowHorizontalLines: true,
			scaleShowVerticalLines: false,
			scaleLineWidth: 1,
			scaleLineColor: "red",
			scaleGridLineColor: "#999",
			scaleDateFormat: "mm/yyyy",
			scaleTimeFormat: "HH:MM",
			scaleDateTimeFormat: "mmm d, yyyy, hh:MM",
			scaleGridLineWidth: 1,
			useUtc: true,
			scaleType: 'date',
			pointDot: false,
			
			scaleOverride: true,
			scaleSteps: 15,
			scaleStepWidth: 0.5,
			scaleStartValue: 29,
			tooltipTemplate: "<%if (datasetLabel){%><%=argLabel%> <%=datasetLabel%>:<%}%><%=valueLabel%>"
			});
		document.getElementById("legend").innerHTML = myChart.generateLegend();
		
	}).error(function(data, status) {
	// Some error occurred
		});
});


